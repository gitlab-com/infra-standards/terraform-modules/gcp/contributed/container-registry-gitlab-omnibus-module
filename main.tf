locals {
  dns_entry         = "${var.instance_name}${var.random_id}.${data.google_dns_managed_zone.dns_zone[0].dns_name}"
  trimmed_dns_entry = trimsuffix(local.dns_entry, ".")
  license_file_name = var.license != "none" ? basename(var.license) : "none"
  license_file_dir  = var.license != "none" ? dirname(var.license) : "scripts"
  elastic_ip        = var.es_server != "none" ? var.es_server.internal_ip : "none"

}

// generate GitLab initial password
resource "random_password" "password" {
  length  = 16
  special = false
}

// generate a static ip address
resource "google_compute_address" "static" {
  address_type = "EXTERNAL"
  description  = "External IP for ${var.instance_description}"
  name         = "ipv4-address-${var.instance_name}${var.random_id}"
  project      = var.gcp_project
  region       = var.gcp_region
}

resource "google_dns_record_set" "a" {
  count        = var.dns_create_record ? 1 : 0
  managed_zone = data.google_dns_managed_zone.dns_zone[0].name
  name         = local.dns_entry
  type         = "A"
  ttl          = var.dns_ttl
  rrdatas      = [google_compute_address.static.address]
}


# Get the existing DNS zone
data "google_dns_managed_zone" "dns_zone" {
  count = var.dns_create_record ? 1 : 0
  name  = var.gcp_dns_zone_name
}

# Create additional disk volume for instance
resource "google_compute_disk" "storage_disk" {
  count  = var.disk_storage_enabled ? 1 : 0
  labels = var.labels
  name   = "${var.instance_name}-storage-disk${var.random_id}"
  size   = var.disk_storage_size
  type   = "pd-ssd"
  zone   = var.gcp_region_zone
}

# Attach additional disk to instance, so that we can move this
# volume to another instance if needed later.
# This will appear at /dev/disk/by-id/google-{NAME}
resource "google_compute_attached_disk" "attach_storage_disk" {
  count       = var.disk_storage_enabled ? 1 : 0
  device_name = "storage-disk"
  disk        = google_compute_disk.storage_disk[0].self_link
  instance    = google_compute_instance.instance.self_link
}

// A single Google Cloud Engine instance
resource "google_compute_instance" "instance" {
  description               = var.instance_description
  deletion_protection       = var.gcp_deletion_protection
  hostname                  = var.dns_create_record ? trimsuffix("${var.instance_name}.${data.google_dns_managed_zone.dns_zone[0].dns_name}", ".") : null
  name                      = "${var.instance_name}${var.random_id}"
  machine_type              = var.gcp_machine_type
  allow_stopping_for_update = "true"
  zone                      = var.gcp_region_zone
  tags                      = var.gcp_network_tags
  labels                    = var.labels
  boot_disk {
    initialize_params {
      type  = "pd-ssd"
      image = var.gcp_image
      size  = var.disk_boot_size
    }
    auto_delete = "true"
  }
  # This ignored_changes is required since we use a separate resource for attaching the disk
  lifecycle {
    ignore_changes = [attached_disk]
  }

  network_interface {
    subnetwork = var.gcp_subnetwork
    access_config {
      // use this to have a static external ip address
      nat_ip = google_compute_address.static.address
    }
  }
  metadata = {
    sshKeys        = "${var.ssh_key_user}:${var.ssh_key}  \n${var.provisioner_user}:${var.provisioner_pub_key}"
    startup-script = var.disk_storage_enabled ? file("${path.module}/init/mnt_dir.sh") : null
    MOUNT_DIR      = var.disk_storage_mount_path
    REMOTE_FS      = "/dev/disk/by-id/google-storage-disk"
  }
  scheduling {
    on_host_maintenance = "MIGRATE"
    automatic_restart   = var.gcp_preemptible ? "false" : "true"
    preemptible         = var.gcp_preemptible
  }
  connection {
    type        = "ssh"
    host        = google_compute_address.static.address
    user        = var.provisioner_user
    private_key = file("${var.provisioner_path}")
    timeout     = var.connection_timeout
  }
  provisioner "file" {
    source      = "${local.license_file_dir}/"
    destination = "/tmp"
  }
  provisioner "file" {
    source      = "${path.module}/scripts/"
    destination = "/tmp"
  }
  provisioner "file" {
    source      = "${path.module}/scripts/${var.script}"
    destination = "/tmp/${var.script}"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/${var.script}",
      "/tmp/${var.script} ${local.trimmed_dns_entry} ${var.gl_version} ${urlencode(nonsensitive(sha256(random_password.password.result)))} ${var.runner_token} ${google_compute_address.static.address} ${var.seed_gl_db} ${var.use_https} ${local.license_file_name} ${local.elastic_ip}"
    ]
  }
}

resource "null_resource" "runner" {

  // if the subnet is "support-resources-subnet" then don't create this resource
  count = var.runner_enabled == true ? 1 : 0
  connection {
    type        = "ssh"
    host        = google_compute_address.static.address
    user        = var.provisioner_user
    private_key = file("${var.provisioner_path}")
    timeout     = var.connection_timeout
  }
  provisioner "file" {
    source      = "${path.module}/scripts/${var.runner_script}"
    destination = "/tmp/${var.runner_script}"
  }
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/${var.runner_script}",
      "/tmp/${var.runner_script} ${var.runner_enabled} ${var.runner_version} ${local.trimmed_dns_entry} ${var.runner_token} ${var.use_https} ",
    ]
  }
  depends_on = [google_compute_instance.instance]
}
