output "public_ip" {
  //description = "Public ip ${var.name}"
  description = "Public ip"
  value       = google_compute_instance.instance.network_interface.0.access_config.0.nat_ip
}

output "internal_ip" {
  description = "Internal ip"
  value       = google_compute_instance.instance.network_interface.0.network_ip
}

output "dns_record" {
  description = "Public DNS"
  value       = trimsuffix("${google_dns_record_set.a[0].name}", ".")
}

output "gitlab_password" {
  description = "The GitLab login password "
  value       = nonsensitive(sha256(random_password.password.result))
}

output "name" {
  description = "The GitLab instance's name"
  value       = var.instance_name
}
