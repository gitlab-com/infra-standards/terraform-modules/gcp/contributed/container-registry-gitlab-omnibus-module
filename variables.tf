variable "provisioner_path" {
  description = "The path to the provisioner key"
}

variable "provisioner_user" {
  type        = string
  description = "The user to attach to the  provisioner ssh key"
  default     = "root"
}

variable "instance_name" {
  type        = string
  description = "The name of the instance to be provisioned"
}

variable "instance_description" {
  type        = string
  description = "Instance description"
  default     = "GitLab troubleshooting instance"
}

variable "gcp_machine_type" {
  type        = string
  description = "The GCP machine type (Example: e2-standard-2)"
  default     = "n1-standard-2"
}

variable "gcp_project" {
  type        = string
  description = "The GCP project ID (may be a alphanumeric slug) that the resources are deployed in. (Example: my-project-name)"
}

variable "ssh_key" {
  type        = string
  description = "The ssh key needed so that the enduser can connect to their resrouce"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDpMAQS3JmF/SmLAgYwQ1ukVczPHqdd5J1X1aLUTL7l85gWvRYFQiBd6m1TuZRNvDlrl/dfqd0PmtX8yGmyHUwaTUWo+qJIJWIvGpIsoiQD30Mkx851AE/t1KReNEhWh4gPcYlMAXBz2xCxMiE77tM19QkYZ9gjcXyewyqazxcUVZhTJLqMfbupTkCodAXDmi7QF/BHAV0RUlfzNkWL5JhDH8H63I7wtwsTru1wI55sfQ+P3VgNxZWqSNSO9ROd1ujhYHj/coZxq+BqhpBXABRuM8VB47wlakcuzHd1mj0aldlVvZaNgNuCgxU3Sk4aYBknMtZ4lGLN4jjcWyVRTkJ5TjZpTEJhT4le7kLpP3nRTpwJu8fQyXoJag10k5qT0tegPRTWHXqpsbcMzYE+PqvaXJGRjyotd0FdOfoeIem3mmnOAsIFZ8O9XozlFtocawtRpZ5N7l2vZQbcGmh/pDpbFRCVlItzLgWNr2141gH1LJNPeBoqMmOUWBcNK1LMWH3jCUMXGUay1S3o4vn72VsTNAYZgdujq5Ujs521ChsV9H7nJjOtTxmSyAZq7nG1deIEWpkjcuNtv5QKcf8REwnSSjK56kBZJb70ekKCR2qMDWGQFj7MRVRrwVlajTdfYIUsFM+7bvGFJ8VwZMbtxhdojhivQwYVXuj4kZDYdLelQ== root@provisioner.support.gitlab"
}

variable "provisioner_pub_key" {
  type        = string
  description = "The provisioner public ssh key"
  default     = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDpMAQS3JmF/SmLAgYwQ1ukVczPHqdd5J1X1aLUTL7l85gWvRYFQiBd6m1TuZRNvDlrl/dfqd0PmtX8yGmyHUwaTUWo+qJIJWIvGpIsoiQD30Mkx851AE/t1KReNEhWh4gPcYlMAXBz2xCxMiE77tM19QkYZ9gjcXyewyqazxcUVZhTJLqMfbupTkCodAXDmi7QF/BHAV0RUlfzNkWL5JhDH8H63I7wtwsTru1wI55sfQ+P3VgNxZWqSNSO9ROd1ujhYHj/coZxq+BqhpBXABRuM8VB47wlakcuzHd1mj0aldlVvZaNgNuCgxU3Sk4aYBknMtZ4lGLN4jjcWyVRTkJ5TjZpTEJhT4le7kLpP3nRTpwJu8fQyXoJag10k5qT0tegPRTWHXqpsbcMzYE+PqvaXJGRjyotd0FdOfoeIem3mmnOAsIFZ8O9XozlFtocawtRpZ5N7l2vZQbcGmh/pDpbFRCVlItzLgWNr2141gH1LJNPeBoqMmOUWBcNK1LMWH3jCUMXGUay1S3o4vn72VsTNAYZgdujq5Ujs521ChsV9H7nJjOtTxmSyAZq7nG1deIEWpkjcuNtv5QKcf8REwnSSjK56kBZJb70ekKCR2qMDWGQFj7MRVRrwVlajTdfYIUsFM+7bvGFJ8VwZMbtxhdojhivQwYVXuj4kZDYdLelQ== root@provisioner.support.gitlab"
}

variable "ssh_key_user" {
  description = "The user to attach to the ssh key"
  default     = "root"
}

variable "gcp_region" {
  type        = string
  description = "The GCP region that the resources will be deployed in. (Ex. eu-west1)"
  default     = "us-central1"
}

variable "gcp_region_zone" {
  type        = string
  description = "The GCP region availability zone that the resources will be deployed in. This must match the region. (Example: eu-west1-c)"
  default     = "us-central1-b"
}

variable "gcp_network_tags" {
  type        = list(any)
  description = "A comma separated array of tags that should be applied to the GCP compute instance that are used for network and firewall rules."
  default     = ["gcp-terraform-interview-instances"]
}

variable "labels" {
  type        = map(any)
  description = "A single-level map/object with key value pairs of metadata labels to apply to the GCP resources. All keys should use underscores and values should use hyphens. All values must be wrapped in quotes."
  default = {
    owner    = "support-team",
    deployed = "terraform"
  }
}

variable "gcp_image" {
  type        = string
  description = "The GCP image name. (Default: ubuntu-2004-focal-v20210720)"
  default     = "ubuntu-2004-focal-v20210720"
}

variable "gcp_dns_zone_name" {
  type        = string
  description = "The GCP Cloud DNS zone name to create instance DNS A record in. This is not the FQDN. (Example: gitlab-sandbox-root-zone)"
}

variable "gcp_subnetwork" {
  description = "The object or self link for the subnet created in the parent module (Example: google_compute_subnetwork.app_subnetwork.self_link)"
}

variable "disk_boot_size" {
  type        = string
  description = "The size in GB of the OS boot volume. (Default: 10)"
  default     = "10"
}

variable "disk_storage_enabled" {
  type        = bool
  description = "True to attach storage disk. False to only have boot disk. (Default: false)"
  default     = "false"
}

variable "disk_storage_mount_path" {
  type        = string
  description = "The Linux directory/path that the storage disk should be mounted to. This will depend on where your application and storage data resides by default. For GitLab Omnibus instances, this will reside in /var/opt, whereas Nginx sites may reside in /srv/www, and other applications may reside in /opt. The default value is chosen based on GitLab deployments being the primary use case of this module. (Default: /var/opt)"
  default     = "/var/opt"
}

variable "disk_storage_size" {
  type        = string
  description = "The size in GB of the storage volume. (Default: 100)"
  default     = "100"
}

variable "dns_create_record" {
  type        = bool
  description = "True to create a DNS record. False to only return an IP address. (Default: true)"
  default     = "true"
}

variable "dns_ttl" {
  type        = string
  description = "TTL of DNS Record for instance. (Default: 300)"
  default     = "300"
}

variable "gcp_deletion_protection" {
  type        = bool
  description = "Enable this to prevent Terraform from accidentally destroying the instance with terraform destroy command. (Default: false)"
  default     = "false"
}

variable "gcp_preemptible" {
  type        = bool
  description = "Enable this to allow this instance to terminate for preemtible reasons. This can cause configuration and data loss. (Default: false)"
  default     = "false"
}

variable "connection_timeout" {
  description = "The timeout to wait for the ssh connection to be established"
  default     = "2m"
}

variable "script" {
  description = "Script to run on gcp instance"
  default     = "installGitLab.sh"
}

variable "gl_version" {
  description = "The GitLab version"
  default     = "latest"
}

variable "seed_gl_db" {
  description = "Whether or not to seed the GitLab with a project"
  default     = true
}

variable "use_https" {
  description = "Whether or not to wait until domain name is resolvable and reconfigure with https"
  default     = false
}

variable "license" {
  description = "The GitLab license to be used by instance"
  default     = "none"
}

variable "es_server" {
  description = "The Elasticsearch instance IP"
  default     = "none"
}

variable "runner_version" {
  description = "The Runner version to install"
  default     = "latest"
}

variable "runner_token" {
  description = "The Runner token to be set"
  default     = "myrunnertoken"
}

variable "runner_script" {
  description = "Script to run on gcp instance"
  default     = "installRunner.sh"
}

variable "runner_enabled" {
  description = "Script to run on gcp instance"
  default     = true
}

variable "vpc_subnet" {
  //description = "The subnet self_link to be used for the resource"
  description = "The subnet name to be used for the resource"
  default     = "support-resources-subnet"
}

variable "random_id" {
  description = "The random id that is passed to resources names to avoid naming conflicts when testing concurent resource creation"
  default     = ""
}
