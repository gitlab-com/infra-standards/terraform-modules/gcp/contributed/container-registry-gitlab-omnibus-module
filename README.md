# Container Registry Team Omnibus GitLab module in GCP

This is the module used for provisioning Omnibus GitLab instances in GCP with terraform.
It also seeds it with a GitLab project with multiple repository and tags on its Container Registry.

## Noteworthy variables

- `gcp_machine_type` - default `n1-standard-2`
- `gcp_region` - default `us-central1`
- `gcp_region_zone` - default `us-central1-b`
- `gl_version` - default `latest`
- `seed_gl_db` - default `true`
- `use_https` - default `true`
- `license` - default `none`
- `es_server` - default `none`
- `runner_version` - default `latest`
