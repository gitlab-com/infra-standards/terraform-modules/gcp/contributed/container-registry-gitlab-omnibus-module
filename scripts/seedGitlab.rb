#!/opt/gitlab/embedded/bin/ruby

require 'gitlab'
require 'faker'

pat = ARGV[0]
ip = ARGV[1]

begin
  response = HTTParty.head("https://#{ip}", verify: false)
  protocol = 'https'
rescue Errno::ECONNREFUSED
  protocol = 'http'
end

client = Gitlab.client(
  endpoint: "#{protocol}://#{ip}/api/v4",
  private_token: pat,
  httparty: {
    verify: false
  }
)

def group
  'demo-group'
end

def my_project
  'demo-project'
end

def gen_description
  Faker::Hacker.say_something_smart
end

path = group.downcase.gsub(/[^0-9A-Za-z]/, '')
my_group = client.create_group(group, path)

options = {
  description: gen_description,
  default_branch: 'master',
  issues_enabled: 1,
  wiki_enabled: 1,
  merge_requests_enabled: 1,
  namespace_id: my_group.id
}

 project = client.create_project(my_project, options)

container_script = <<~SHELL
  #!/bin/bash
  path="/demogroup/demo-project"
  tags=${1:-2}
  repos=${2:-1}
  base=$3

  tag_names[0]="latest"
  tag_names[1]="stable"
  tag_names[2]="beta"
  tag_names[3]="alpha"
  tag_names[4]="next"

  image_names[0]="docker"
  image_names[1]="gitlab"
  image_names[2]="git"
  image_names[3]="ruby"
  image_names[4]="rails"
  image_names[5]="node"
  image_names[6]="npm"

  tag_names_size=${#tag_names[@]}
  image_names_size=${#image_names[@]}

  for ((j =0; j< $repos; j++))
  do
      IMAGE_BASE_INDEX=$(($RANDOM % $tag_names_size))
      IMAGE_NAME="${image_names[$IMAGE_BASE_INDEX]}"-$RANDOM
      for ((i =0; i< $tags; i++))
      do
          TAG_BASE_INDEX=$(($RANDOM % $tag_names_size))
          TAG=$RANDOM
          TAG_NAME="${tag_names[$TAG_BASE_INDEX]}"-"$TAG"
          echo $TAG > seed
          echo
          docker build -t "$base$path"/"$IMAGE_NAME":$TAG_NAME .
          docker push "$base$path"/"$IMAGE_NAME":$TAG_NAME
      done
  done
SHELL

container_generator_yml = <<~YAML
  build:
    image: docker:19.03.12
    stage: build
    services:
      - docker:19.03.12-dind
    variables:
      IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
    script:
      - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
      - apk add --no-cache --upgrade bash
      - chmod +x containerRegistryFactory.sh
      - /bin/bash containerRegistryFactory.sh 2 10 $CI_REGISTRY 
YAML

dockerfile = <<~DOCKER
  FROM scratch
  ADD seed /
DOCKER

client.create_file(project.id, 'containerRegistryFactory.sh', 'main', container_script, 'Initial commit')
client.create_file(project.id, '.gitlab-ci.yml', 'main', container_generator_yml, 'Initial commit')
client.create_file(project.id, 'Dockerfile', 'main', dockerfile, 'Initial commit')