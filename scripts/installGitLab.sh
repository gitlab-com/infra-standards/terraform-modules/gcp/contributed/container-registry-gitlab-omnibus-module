#!/bin/bash
# because cloud-init is configured to run on standard Ubuntu images, and the provisioner is attempting to run while cloud-init is also running, we're running into a timing/conflic
# so need to wait for cloud-init to finish
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
sleep 1
done

short_host_name=$1
gl_version=$2
gl_password=$3
runner_token=$4
ip=$5
seed_gl_db=$6
https=$7
license_path_repo=$8
host_name="http://$short_host_name"
elastic=$9

sudo apt-get update;
sudo DEBIAN_FRONTEND=noninteractive sudo apt-get -y upgrade;

sudo DEBIAN_FRONTEND=noninteractive apt-get -y install vim curl openssh-server ca-certificates;
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install postfix
sudo curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash;


if [ "$license_path_repo" != "none" ]; then
      #get the name of the license 
      #IFS='/' read -ra license_arr <<< "$license_path_repo"

      #len=${#license_arr[@]}

      #license_path="/tmp/${license_arr[len-1]}"
      license_path="/tmp/${license_path_repo}"
fi


echo "Passed hostname is $host_name"
echo "GitLab version is $gl_version"

# if no version is provided use latest else use the provided version
if [ "$gl_version" != "latest" ]
then
      # changing formatting of version from tag of official repo to apt-get repo version
      # For example: "v13.12.0-ee" will be changed to "13.12.0-ee.0"
      gl_version="${gl_version:1}.0"
      echo "Formatted GitLab version is $gl_version"

      if [ "$license_path_repo" != "none" ]; then
            echo "Command that ran: sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token GITLAB_LICENSE_FILE=$license_path apt-get --yes --force-yes install gitlab-ee=$gl_version;"
            sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token GITLAB_LICENSE_FILE=$license_path apt-get --yes --force-yes install gitlab-ee=$gl_version;
      else
            echo "Command that ran: sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token apt-get --yes --force-yes install gitlab-ee=$gl_version;"
            sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token apt-get --yes --force-yes install gitlab-ee=$gl_version;
      fi
else
      if [ "$license_path_repo" != "none" ]; then
            echo "Command that ran: sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token GITLAB_LICENSE_FILE=$license_path apt-get --yes --force-yes install gitlab-ee;"
            sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token GITLAB_LICENSE_FILE=$license_path apt-get --yes --force-yes install gitlab-ee;
      else
            echo "Command that ran: sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token apt-get --yes --force-yes install gitlab-ee;"
            sudo EXTERNAL_URL=$host_name GITLAB_ROOT_PASSWORD=$gl_password GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=$runner_token apt-get --yes --force-yes install gitlab-ee;
      fi
fi

echo "Have determined that seed_gl_db is $seed_gl_db"

if $seed_gl_db
then
      echo "Disabling GitLab registration and creating api token for seeding GitLab DB"

      token=`/opt/gitlab/bin/gitlab-rails runner "ApplicationSetting.last.update(signup_enabled: false); pat = User.find_by_username('root').personal_access_tokens.create( name: 'seed_apitoken',  impersonation: false,  scopes: [:api]); puts pat.token"`
      echo "Disabled GitLab registration."
      echo "The token is: $token"

      /opt/gitlab/embedded/bin/gem install gitlab faker

      attempt_counter=0
      max_attempts=18

      echo "Waiting for GitLab to respond..."

      until $(curl --output /dev/null --silent --head --fail http://$ip); do

            if [ $attempt_counter -eq $max_attempts ];then
                  echo "GitLab is not yet up, this is taking too long, giving up on seeding..."
                  exit 0
            fi

            echo '...'
            let "attempt_counter+=1"
            sleep 5
      done

      echo "Hostname: $host_name"

      echo "IP: $ip"

      echo "Seeding GitLab..."

      /opt/gitlab/embedded/bin/ruby /tmp/seedGitlab.rb $token $ip
else
      # chosen to split the registering depending on seeding since it's a wee bit faster when seeding
      echo "Disabling GitLab registration..."
      # https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/2837#note_109602925
      /opt/gitlab/bin/gitlab-rails runner 'ApplicationSetting.last.update(signup_enabled: false)'
      echo "Disabled GitLab registration."
      echo "Chosen not to seed the GitLab instance."
fi

if $https
then
      echo "Chosen to run script that will try to reconfigure with https 1 minute after the server is created."  
      # will run the next line in about 1 min so terraform has a chance to complete the instance creation and create the dns entry
      echo "until ping -c1 $short_host_name >/dev/null 2>&1; do sleep 1; done && sed -i -e 's/external_url '\''http/external_url '\''https/' -e 's/# letsencrypt\['\''enable'\''\] = nil/letsencrypt\['\''enable'\''\] = true/' /etc/gitlab/gitlab.rb && gitlab-ctl reconfigure" > /tmp/reconfigureLine.log
      echo "until ping -c1 $short_host_name >/dev/null 2>&1; do sleep 1; done && sed -i -e 's/external_url '\''http/external_url '\''https/' -e 's/# letsencrypt\['\''enable'\''\] = nil/letsencrypt\['\''enable'\''\] = true/' /etc/gitlab/gitlab.rb && gitlab-ctl reconfigure" | at now + 1 minute -q a

      echo "Please check /var/log/gitlab/reconfigure if you can't reach $host_name on port 443" 
else
      echo "Not using https."
fi

if [ "$elastic" != "none" ]
then
      if [ $license_path_repo == "none" ]
      then 
            echo "You did not provide a license path, so there will be no attempt to perform any of the Elasticsearch tasks."
            exit 0
      fi 
      echo "Chosen to configure Global Search (Elasticsearch)." 
      echo "Elasticsearch server IP: ${elastic}"
      echo "Setting-up Elasticsearch will only work if ${license_path} is provided and is valid."
      echo "Setting Elasticsearch integration URL to: http://${elastic}:9200"
      /opt/gitlab/bin/gitlab-rails runner "ApplicationSetting.last.update(elasticsearch_url: 'http://${elastic}:9200')"
      if [ $? -ne 0 ]
            then
                  echo "Failed when setting Elasticsearch integration URL (UI text field)"
                  exit 1
      fi
      echo "Create empty index"
      gitlab-rake gitlab:elastic:create_empty_index
      if [ $? -ne 0 ]
            then
                  echo "Failed when trying to create Elasticsearch empty index (rake task)"
                  exit 1
      fi
      echo "Enabling Indexing (UI checkbox)"
      /opt/gitlab/bin/gitlab-rails runner "ApplicationSetting.last.update(elasticsearch_indexing: true)"
      if [ $? -ne 0 ]
            then
                  echo "Failed when checking box Elasticsearch Indexing (UI checkbox)"
                  exit 1
      fi
      echo "Clear index status"
      gitlab-rake gitlab:elastic:clear_index_status
      if [ $? -ne 0 ]
            then
                  echo "Failed when clearing index status (rake task)"
                  exit 1
      fi
      echo "Indexing..."
      gitlab-rake gitlab:elastic:index_projects
      if [ $? -ne 0 ]
            then
                  echo "Failed when trying to index (rake task)"
                  exit 1
      fi
      echo "Indexing snippets..."
      gitlab-rake gitlab:elastic:index_snippets
      if [ $? -ne 0 ]
            then
                  echo "Failed when trying to index snippets (rake task)"
                  exit 1
      fi
      echo "Enabling searching with Elastingsearch (UI checkbox)"
      /opt/gitlab/bin/gitlab-rails runner "ApplicationSetting.last.update(elasticsearch_search: true)"
      if [ $? -ne 0 ]
            then
                  echo "Failed when checking box Search with Elasticsearch enabled (UI checkbox)"
                  exit 1
      fi
else
      echo "Not using Global Search (Elasticsearch)."
fi