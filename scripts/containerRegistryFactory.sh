#!/bin/bash
path="/demogroup/demo-project"
tags=${1:-2}
repos=${2:-1}
base=$3
user=$4
password=$5

tag_names[0]="latest"
tag_names[1]="stable"
tag_names[2]="beta"
tag_names[3]="alpha"
tag_names[4]="next"

image_names[0]="docker"
image_names[1]="gitlab"
image_names[2]="git"
image_names[3]="ruby"
image_names[4]="rails"
image_names[5]="node"
image_names[6]="npm"

tag_names_size=${#tag_names[@]}
image_names_size=${#image_names[@]}

for ((j =0; j< $repos; j++))
do
    IMAGE_BASE_INDEX=$(($RANDOM % $tag_names_size))
    IMAGE_NAME="${image_names[$IMAGE_BASE_INDEX]}"-$RANDOM
    for ((i =0; i< $tags; i++))
    do
        TAG_BASE_INDEX=$(($RANDOM % $tag_names_size))
        TAG=$RANDOM
        TAG_NAME="${tag_names[$TAG_BASE_INDEX]}"-"$TAG"
        echo $TAG > seed
        echo
        docker build -t "$base$path"/"$IMAGE_NAME":$TAG_NAME /tmp
        docker push "$base$path"/"$IMAGE_NAME":$TAG_NAME
    done
done