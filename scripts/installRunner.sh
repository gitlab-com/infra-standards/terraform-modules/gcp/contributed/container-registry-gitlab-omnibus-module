#!/bin/bash

# because cloud-init is configured to run on standard Ubuntu images, and the provisioner is attempting to run while cloud-init is also running, we're running into a timing/conflic
# so need to wait for cloud-init to finish
until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
sleep 1
done

#Exit on first error
set -eu

if [ $1 == "false"  ]; then exit 0
fi

runner_version=$2
gl_name=$3
runner_token=$4

if [ $5 == "true" ]; then
 proto="https"
else
 proto="http"
fi

sudo apt-get update;
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install vim curl openssh-server ca-certificates apt-transport-https gnupg-agent software-properties-common;
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

docker volume create gitlab-runner-config

if docker run -d --name gitlab-runner --restart always -v /var/run/docker.sock:/var/run/docker.sock -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:$runner_version ; 
  then
    echo "Docker image for runner was successfully installed!"
  else
    echo "Could not install Docker image for Runner. Proceeding without Runner!"
    exit 0
fi

# Initializing counter to timeout registration if not succesful for 5 minutes
count=0

while ! docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:$runner_version register --non-interactive --run-untagged --url "$proto://$gl_name" --registration-token "$runner_token" --description 'Docker Runner on same node' --tag-list 'test,docker,prod' --executor 'docker' --docker-image 'docker:stable' --locked=false --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" &> /dev/null
  do
    count=$(($count + 1))
    if [ $count -eq 60 ]; then
      echo "Runner could not be registered after 10 minutes. Proceeding without Runner!"
      exit 0
    fi
    echo "Sleeping for 10 seconds until runner registration is tried again!"
    sleep 10
  done

echo "Runner has been installed and registered succesfully!"